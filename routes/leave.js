const express = require("express")
const bcrypt = require("bcryptjs")
const router = express.Router()
const Leave = require("../models/Leave")
let jwt = require('jsonwebtoken');
let middleware = require('../config/middleware');
const User = require("../models/User");

import { sendMail } from '../helper/sendMail';

router.post('/add', async(req, res) => {
    const { user, start_date, end_date, duration, type, msg } = req.body.details;
    console.log(req.body)
    const newLeave = new Leave({
        user,
        start_date,
        end_date,
        duration,
        type,
        msg
    })

    User.findById((user), (err, user) =>{
        console.log("user",user)
        const depart = user.department
        User.findOne({ 'role': 'PSADMIN', 'department': depart}, (err, adminUser) => {
            const email = adminUser.email
            const emailSub = 'Leave Notification'
            const emailData = user.name + ', applied for the leave'
            sendMail(email, emailSub, emailData).then(mailRes=>{
                console.log("mailres", mailRes)
            }).catch(mailErr=>{
                console.log("err",mailErr)
            })
            //send email to admin user of that department
        })
    })
    await newLeave.save()
    .then(leave => {
        res.status(201).json({
            msg: "Your leave is inserted",
            leave
        })
    })  
})

router.post('/update',  (req, res) => {
    //find by id and update
    const { id, user, start_date, end_date, duration, type } = req.body;

    Leave.findByIdAndUpdate(id, {
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        duration: req.body.duration,
        type: req.body.type
    })
    .then(user => {
        res.status(200).json({
            msg: "Your leave is updated",
            user
        })
    })  
})

router.post('/delete', (req, res) => {
    //find by id and update
    // const { id, user, start_date, end_date, duration, type } = req.body;
    console.log("delete 123",req.body.x.id)

    // Leave.deleteOne({ _id: req.body.x.id });
    Leave.remove({ _id: req.body.x.id }, function(err,leave) {
        if (!err) {
            console.log("delete success",req.body.x.id)
                res.status(200).json({
                    msg: "Deleted Successfully!",

                })
        }
        else {
            console.log("delete error",req.body.x.id)
            res.status(400).json({
                msg: "Error",

            })
        }
    });


})

router.post('/status',  (req, res) => {
    //find by leave id and update
    const { id } = req.body.x;
    console.log("***status*****",req.body.x)

    Leave.findByIdAndUpdate(id, {
        status: req.body.x.status
    })
    .then(user => {
        res.status(200).json({
            msg: "Your leave status is updated",
            user
        })
    })  
})

router.post('/list',  async(req, res) => {
    //find by user id and get list
    console.log("***********in list**************",req.body)
    const { id } = req.body;
    User.findById((id), (err, user) => {
        if (err) {
            res.status('403').send({
                success: false,
                message: 'You are not Authorize'
            });
        }
        console.log(user)
        if(user.role == 'PSADMIN') {


            
    Leave.find({ })
    .populate('user').exec((err, leaves) => {
                if (err) {
                    res.status('403').send({
                        success: false,
                        message: 'You are not Authorize'
                    });
                }
        
                res.status('200').send({ 
                    success: true,
                    user: leaves
                })
            })
        } else {
            Leave.find({ user: userId }, (err, leaves) => {
                if (err) {
                    res.status('403').send({
                        success: false,
                        message: 'You are not Authorize'
                    });
                }
        
                res.status('200').send({ 
                    success: true,
                    user: leaves
                })
            })
        }
    })
    // Leave.find({ user: id }, (err, leaves) => {
    //     if (err) {
    //         res.status('403').send({
    //             success: false,
    //             message: 'You are not Authorize'
    //         });
    //     }

    //     res.status('200').send({ 
    //         success: true,
    //         user: leaves
    //     })
    // })
})

module.exports = router