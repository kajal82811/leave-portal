const express = require("express")
const bcrypt = require("bcryptjs")
const router = express.Router()
const User = require("../models/User")
let jwt = require('jsonwebtoken');
let middleware = require('../config/middleware');

const { ObjectId } = require('mongodb');

const app = express()

router.post('/register', (req, res) => {
    // console.log('coming inside register function')
    // res.status(200).json({
    //     body: req.body
    // })

    const { name, password, company, phone, dob, doj, department,role } = req.body;

    const email = req.body.email.trim().toLowerCase();

    let errors = []

    if (!name || !email || !password || !role || !company) {
        errors.push({ msg: 'Please fill all the fields' })
    }

    if (password.length < 6 ) {
        errors.push({ msg: "Password should be atleast 6 characters" })
    }

    if (errors.length > 0) {
        res.status(400).json({ errors })
    } else {
        User.findOne( {email: email} )
        .then( (user) => {
            if (user) {
                errors.push( { msg: "Email is already registered" })
                res.status(400).json({ errors })
            } else {
                const newUser = new User({
                    name,
                    email,
                    password,
                    company,
                    phone,
                    dob,
                    doj,
                    role,
                    department
                })
                
                //hashed password
                bcrypt.genSalt(8, (error, salt) => {
                    bcrypt.hash(newUser.password, salt, async (error, hash) => {
                        if(error) {
                            errors.push( { msg: "Password hashing issue" })
                            res.status(400).json({ errors })
                        }
                        newUser.password = hash
                        await newUser.save()
                        .then(user => {
                            res.status(201).json({
                                msg: "You are now registered and can log in",
                                user
                            })
                        })
                        .catch(err => console.log(err))
                    })
                })
            }
        });
    }
});

router.post('/login', middleware.findUserByCredentials, async (req, res, next) => {
    if (req.user) {
        console.log("in user login")
        console.log(req.user)
        
        let token = jwt.sign({ email: req.body.email.trim().toLowerCase() },
            process.env.ACCESS_TOKEN_SECRET,
            { 
                expiresIn: '24h' // expires in 24 hours
            }
        );

        res.status('200').send({ 
            success: true,
            message: 'Authentication successful!',
            token,
            user: req.user
        })

    } else {
        res.status('403').send({
            success: false,
            message: 'Incorrect username or password'
        });
    }


})

router.post('/getUsers', middleware.checkToken, async (req, res) => {
    const user_id = req.body.user_id
    if (user_id) {
        User.findById(user_id, (err, user) => {
            if(err) console.log(err)
            // console.log(user.role)
            if(user.role == 'PSADMIN') {
                User.find({}, (err, users) => {
                    if (err) {
                        res.status('403').send({
                            success: false,
                            message: 'You are not Authorize'
                        });
                    }
        
                    res.status('200').send({ 
                        success: true,
                        user: users
                    })
                })
            } else {
                res.status('403').send({
                    success: false,
                    message: 'You are not Admin'
                });
            }
        })
        
    } else {
        res.status('403').send({
            success: false,
            message: 'Please pass UserID'
        });
    }
})

module.exports = router