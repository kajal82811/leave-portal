
// export const getModelForDb = (databaseName, model) =>{
//     const db = mongoose.connection.useDb(databaseName)
//     return db.model(model.modelName, model.schema)
// }


const express       = require('express')
const path          = require('path')
const app           = express()
const mongoose      = require('mongoose')
const bodyParser    = require('body-parser')

import AppConfig from './config/AppConfig';
import {API_PORT, BASE_URL, MONGO_URI} from './config/constant';


// console.log(MONGO_URI)
// mongoose.connect(MONGO_URI, { 
//     useNewUrlParser: true, 
//     useUnifiedTopology: true 
// })

mongoose.connect(MONGO_URI, { useNewUrlParser: true, useFindAndModify: false })
    .then(() => console.log('MongoDB Connected...'))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static("front-build/kajal"));
app.use(express.urlencoded({ extended: false}))
app.use('/user', require('./routes/user'))
app.use('/leave', require('./routes/leave'))


app.get('*', (req, res) => {
    console.log('coming inside server file')
    console.log(req.headers.host);
    let host_config = AppConfig.host_config[req.headers.host];
    console.log(host_config);
    if(host_config.react_build){
        res.sendFile(path.resolve(__dirname, "front-build",host_config.react_build, "index.html"));
    }
    else{
        res.status(404).send({success:false,message:`react_build not defined for host: ${req.headers.host}`})
    }
});

app.listen(API_PORT, (error, response) => {
    if(error) console.error('Server isn\'t running ' + error)
    else console.log('Server is running on port ' + API_PORT)
})
