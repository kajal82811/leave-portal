const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        // required: true
    },
    company: { 
        // type: Schema.Types.ObjectId, 
        // ref: 'Company',
        // required: true 
        type: String,
        default: "PROPSTORY"
        // required: true
    },
    enable: {
        type: Boolean,
        default: false
    },
    role:{
        type: String,
        default: 'TEAM-MEMBER'
    },
    token: {
        type: String,
        default: ''
    },
    department: {
        type: String,
        required:true
    },
    confirmation: {
        type: Boolean,
        default: false
    },
    doj: {
        type: Date,
        default: ''
    },
    dob: {
        type: Date,
        default: ''
    },
    lastLogin: {
        type: Date,
        default: ''
    },
    modifiedDate: {
        type: Date,
        default: Date.now
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

const User = mongoose.model('User', UserSchema)

module.exports = User