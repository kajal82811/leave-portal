const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const LeaveSchema = new mongoose.Schema({
    user: { 
        type: Schema.Types.ObjectId, 
        ref: 'User',
        required: true 
    },
    type: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'Pending'
    },
    msg: {
        type: String
    },
    duration: {
        type: String,
       
    },
    start_date: {
        type: Date,
        default: Date.now
    },
    end_date: {
        type: Date,
        default: Date.now
    },
    created_date: {
        type: Date,
        default: Date.now
    },
});

const Leave = mongoose.model('Leave', LeaveSchema)

module.exports = Leave