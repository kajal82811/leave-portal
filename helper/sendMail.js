import nodemailer from 'nodemailer';
import AppConfig from '../config/AppConfig';
import { sesMailSender } from './sesMailSender';
// import { sendGridMailSend } from './sendGridMail';
// import { smtpMailSend } from './smtpMailSend';

export const sendMail = async  (toEmail,subject,body,attachments) =>{
    if(AppConfig.mail_type == 'SES'){
       let res = await sesMailSender(toEmail,subject,body)
       return res
    }
    // else if(AppConfig.mail_type == 'SENDGRID'){
    //     let res = await sendGridMailSend(toEmail,subject,body,attachments)
    //     return res
    // }
    // else if(AppConfig.mail_type == 'SMTP'){
    //     let res = await smtpMailSend(toEmail,subject,body,attachments)
    //     return res
    // } 
}