import { SES_CONFIG, SES_SOURCE_EMAIL } from '../config/constant';
const AWS = require("aws-sdk");

AWS.config.update(SES_CONFIG);

const ses = new AWS.SES({ apiVersion: "2010-12-01" });

export const sesMailSender = async  (toEmail, subject, body) =>{
    const params = {
        Destination: {
          CcAddresses: ['rahul@propstory.com'],
          ToAddresses: [toEmail] // Email address/addresses that you want to send your email
        },
        Message: {
          Body: {
            Html: {
              Charset: "UTF-8",
              Data: body
            },
            Text: {
              Charset: "UTF-8",
              Data: body
            }
          },
          Subject: {
            Charset: "UTF-8",
            Data: subject
          }
        },
        Source: SES_SOURCE_EMAIL
      };
      
      const sendEmail = ses.sendEmail(params).promise();
      return sendEmail   
}